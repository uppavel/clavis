# clavis

### Description

Clavis is a Python library. It provides a way to use db transaction from SqlAlchemy as a context manager.

![](https://img.shields.io/pypi/pyversions/clavis.svg?style=popout)
![](https://img.shields.io/pypi/implementation/clavis.svg?style=popout)
![](https://img.shields.io/pypi/wheel/clavis.svg?style=popout)
![](https://img.shields.io/pypi/status/clavis.svg?style=popout)
![](https://img.shields.io/pypi/l/clavis.svg?style=popout)
![](https://img.shields.io/pypi/dm/clavis.svg?style=popout)
![](https://img.shields.io/gitlab/pipeline/tgrx/clavis/master.svg?style=popout)
[![coverage report](https://gitlab.com/tgrx/clavis/badges/master/coverage.svg)](https://gitlab.com/tgrx/clavis/commits/master)
[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

### Restrictions

Current DB support:

- PostgreSQL


### Examples

TBD